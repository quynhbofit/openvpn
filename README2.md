## 1. On GCP Compute

### B1 Open port UDP openvpn on Google Firewalld

### B2. Config iptables

```
-A INPUT -p udp -m state --state NEW -m udp --dport 1994 -j ACCEPT
-A FORWARD -i tun0 -j ACCEPT
-A FORWARD -i eth0 -o tun+ -m state --state RELATED,ESTABLISHED -j ACCEPT
```

### B3. Edit file /etc/sysctl.conf

```
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
```

## On CMC (Don't set firewall CMC)

### B1. Config iptables

```
-A INPUT -p udp -m state --state NEW -m udp --dport 1994 -j ACCEPT
-A FORWARD -i tun0 -j ACCEPT
-A FORWARD -i eth0 -o tun+ -m state --state RELATED,ESTABLISHED -j ACCEPT
```

### B2. Edit file /etc/sysctl.conf

```
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
```